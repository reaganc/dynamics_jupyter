[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fbitbucket.org%2Freaganc%2Fdynamics_jupyter/HEAD?urlpath=tree)

Click the badge above to launch the Jupyter notebooks in this git repository on binder.
