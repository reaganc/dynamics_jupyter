# Import required modules
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as ani
import ipywidgets as wid
import scipy.interpolate

# Define plot attributes
plt.rcParams['figure.figsize'] = (12, 12) # Does not seem to work on Azure for some reason
plt.rcParams['font.size'] = 16
plt.rcParams['axes.spines.top'] = False
plt.rcParams['axes.spines.right'] = False
plt.rcParams['axes.labelsize'] = 'large'
plt.rcParams['xtick.labelsize'] = 'small'
plt.rcParams['ytick.labelsize'] = 'small'
plt.rcParams['animation.html'] = 'jshtml'

"""
Compute the response of an undamped SDOF system to a rectangular pulse load

T_n    : Natural period
t_d    : Duration of pulse load
t_max  : Time until which to compute response
numpts : Number of points to discretise time array
"""
def compute_response(T_n, t_d, t_max, numpts):

    # Compute the natural angular frequency
    omega_n = 2*np.pi/T_n
    
    # Define the time domain
    t = np.linspace(0, t_max, numpts)
    idx_forced = np.flatnonzero(t <= t_d)
    idx_free = np.flatnonzero(t > t_d)
    
    # Compute the static response
    ust0 = np.zeros(len(t))
    ust0[idx_forced] = 1
    
    # Compute the dynamic response
    u_ust0 = np.empty(len(t))
    u_ust0[idx_forced] = 1 - np.cos(omega_n*t[idx_forced])
    u_ust0[idx_free] = 2*np.sin(omega_n*t_d/2)*np.sin(omega_n*(t[idx_free] - t_d/2))
    
    return t, ust0, u_ust0

"""
Plot and animate the response of an undamped SDOF system to a rectangular pulse load

T_n  : Natural period
t_d  : Duration of pulse load
anim_flag : Flag to animate the response
"""
def animate_response(T_n, t_d, anim_flag):
    
    # Compute the response
    t_max = 4
    numpts = 1000
    t, ust0, u_ust0 = compute_response(T_n, t_d, t_max, numpts)
    
    # Plot the static and dynamic response time histories
    fig = plt.figure(figsize=(12, 12))
    ax1 = plt.subplot2grid((3, 1), (0, 0))
    ax1.plot(t, ust0, color='k', lw=2, linestyle='dashed', zorder=0)
    ax1.plot(t, u_ust0, color='#BF1C1C', lw=2, zorder=1)
    ax1.grid(True)
    ax1.set_xlim(0, t_max)
    ax1.set_ylim(-2.2, 2.2)
    ax1.set_yticks((-2, -1, 0, 1, 2))
    ax1.set_xlabel(r'$t$')
    ax1.set_ylabel(r'$u(t)/(u_{st})_0$')
        
    plt.tight_layout(pad=0.5)

    # Animate the response if the toggle button is pressed
    if anim_flag:

        # Plot one frame of the animation
        def plot_frame(frame_num):

            # Plot the point moving over the response curve
            lines[0].set_data(t_anim[frame_num], u_anim[frame_num])

            # Plot the lollipop
            numpts = 20
            y = np.linspace(0, 1, numpts)
            x = u_anim[frame_num]*(-2*y**3 + 3*y**2) + u_anim[frame_num]*1.5*y*(y**2 - y)
            lines[1].set_data(x, y)
            lines[2].set_data(u_anim[frame_num], 1)

            return lines

        # Initialise the axes
        ax2 = plt.subplot2grid((3, 1), (1, 0), rowspan=2)
        xmax = max(u_ust0)*5
        ax2.set_xlim(-xmax, xmax)
        ax2.set_ylim(-0.3, 1.3)
        ax2.spines['left'].set_visible(False)
        ax2.spines['bottom'].set_visible(False)
        ax2.set_xticks(())
        ax2.set_yticks(())
        
        # Create the dummy handles
        lines = []
        lines += ax1.plot([], [], color='k', marker='o', markersize=10, lw=0, zorder=2)
        lines += ax2.plot([], [], color='k', lw=2, zorder=0)
        lines += ax2.plot([], [], color='k', marker='o', markersize=60, markeredgewidth=2, \
                markerfacecolor='w', zorder=1)
        plt.close()
        
        # Animate the response
        fps = 30
        numpts_anim = t_max*fps
        t_anim = np.linspace(0, t_max, numpts_anim)
        u_anim = scipy.interpolate.interp1d(t, u_ust0)(t_anim)
        animation = ani.FuncAnimation(fig, plot_frame, frames=numpts_anim, interval=1000/fps, blit=True, \
                repeat=False)
        return animation
