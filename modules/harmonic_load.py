# Import required modules
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as ani
import ipywidgets as wid
import scipy.interpolate

# Define plot attributes
plt.rcParams['figure.figsize'] = (12, 17) # Does not seem to work on Azure for some reason
plt.rcParams['font.size'] = 16
plt.rcParams['axes.spines.top'] = False
plt.rcParams['axes.spines.right'] = False
plt.rcParams['axes.labelsize'] = 'large'
plt.rcParams['xtick.labelsize'] = 'small'
plt.rcParams['ytick.labelsize'] = 'small'
plt.rcParams['animation.html'] = 'jshtml'

"""
Compute the response of an underdamped SDOF system to a harmonic load

T_n    : Natural period
T      : Period of harmonic load
zeta   : Damping ratio
u0     : Initial displacement
v0     : Initial velocity
t_max  : Time until which to compute response
numpts : Number of points to discretise time array
"""
def compute_response(T_n, T, zeta, u0, v0, t_max, numpts):

    # Compute the damped natural angular frequency and angular frequency of the load
    omega_n = 2*np.pi/T_n
    omega_D = omega_n*np.sqrt(1 - zeta**2)
    omega = 2*np.pi/T
    beta = omega/omega_n

    # Define the time domain
    t = np.linspace(0, t_max, numpts)

    # Compute the transient response
    u_st0 = 1
    if zeta == 0 and beta == 1:
        u_c = u0*np.cos(omega_n*t) + (v0/omega_n + u_st0/2)*np.sin(omega_n*t)
    else:
        factor = u_st0/((1 - beta**2)**2 + (2*zeta*beta)**2)
        C_1 = u0 + factor*2*zeta*beta
        C_2 = (v0 + zeta*omega_n*u0)/omega_D + factor/omega_D*(2*zeta**2*beta*omega_n - (1 - beta**2)*omega)
        u_c = (C_1*np.cos(omega_D*t) + C_2*np.sin(omega_n*t))*np.exp(-zeta*omega_n*t)

    # Compute the steady-state response
    if zeta == 0 and beta == 1:
        u_p = -u_st0/2*omega_n*t*np.cos(omega_n*t)
    else:
        u_p = factor*((1 - beta**2)*np.sin(omega*t) - 2*zeta*beta*np.cos(omega*t))
    
    # Compute the total response
    u = u_c + u_p
    
    return t, u_c, u_p, u

"""
Plot and animate the response of an underdamped SDOF system to a harmonic load

T_n       : Natural period
T         : Period of harmonic load
zeta      : Damping ratio
u0        : Initial displacement
v0        : Initial velocity
anim_flag : Flag to animate the response
"""
def animate_response(T_n, T, zeta, u0, v0, anim_flag):
    
    # Compute the response
    t_max = 4
    numpts = 1000
    t, u_c, u_p, u = compute_response(T_n, T, zeta, u0, v0, t_max, numpts)
    
    # Compute plot attributes
    ymax = max(abs(min(u_c)), abs(max(u_c)), abs(min(u_p)), abs(max(u_p)), abs(min(u)), abs(max(u)))
    
    # Plot the transient response time history
    fig = plt.figure(figsize=(12, 17))
    ax1 = plt.subplot2grid((5, 1), (0, 0))
    ax1.plot(t, u_c, color='k', linestyle='dashed', lw=2)
    ax1.grid(True)
    ax1.set_xlim(0, t_max)
    ax1.set_ylim(1.2*np.array([-ymax, ymax]))
    ax1.set_xlabel(r'$t$')
    ax1.set_ylabel(r'$u_c(t)/(u_{st})_0$')
    ax1.set_title('Transient response')
    
    # Plot the steady-state response time history
    ax2 = plt.subplot2grid((5, 1), (1, 0))
    ax2.plot(t, u_p, color='0.5', linestyle='dashed', lw=2)
    ax2.grid(True)
    ax2.set_xlim(0, t_max)
    ax2.set_ylim(1.2*np.array([-ymax, ymax]))
    ax2.set_xlabel(r'$t$')
    ax2.set_ylabel(r'$u_p(t)/(u_{st})_0$')
    ax2.set_title('Steady-state response')

    # Plot the total response time history
    ax3 = plt.subplot2grid((5, 1), (2, 0))
    ax3.plot(t, u, color='#BF1C1C', lw=2, zorder=1)
    ax3.grid(True)
    ax3.set_xlim(0, t_max)
    ax3.set_ylim(1.2*np.array([-ymax, ymax]))
    ax3.set_xlabel(r'$t$')
    ax3.set_ylabel(r'$u(t)/(u_{st})_0$')
    ax3.set_title('Total response')
    
    plt.tight_layout(pad=0.5)
    
    # Animate the response if the toggle button is pressed
    if anim_flag:

        # Plot one frame of the animation
        def plot_frame(frame_num):

            # Plot the point moving over the response curve
            lines[0].set_data(t_anim[frame_num], u_anim[frame_num])

            # Plot the lollipop
            numpts = 20
            y = np.linspace(0, 1, numpts)
            x = u_anim[frame_num]*(-2*y**3 + 3*y**2) + u_anim[frame_num]*1.5*y*(y**2 - y)
            lines[1].set_data(x, y)
            lines[2].set_data(u_anim[frame_num], 1)

            return lines

        # Initialise the axes
        ax4 = plt.subplot2grid((5, 1), (3, 0), rowspan=2)
        xmax = max(u)*5
        ax4.set_xlim(-xmax, xmax)
        ax4.set_ylim(-0.3, 1.3)
        ax4.spines['left'].set_visible(False)
        ax4.spines['bottom'].set_visible(False)
        ax4.set_xticks(())
        ax4.set_yticks(())
        
        # Create the dummy handles
        lines = []
        lines += ax3.plot([], [], color='k', marker='o', markersize=10, lw=0, zorder=1)
        lines += ax4.plot([], [], color='k', lw=2, zorder=0)
        lines += ax4.plot([], [], color='k', marker='o', markersize=60, markeredgewidth=2, \
                markerfacecolor='w', zorder=1)
        plt.close()
        
        # Animate the response
        fps = 30
        numpts_anim = t_max*fps
        t_anim = np.linspace(0, t_max, numpts_anim)
        u_anim = scipy.interpolate.interp1d(t, u)(t_anim)
        animation = ani.FuncAnimation(fig, plot_frame, frames=numpts_anim, interval=1000/fps, blit=True, \
                repeat=False)
        return animation
